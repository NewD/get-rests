const fs = require('fs');
const https = require('https');
const { checkDirectoryExisting } = require('../helpers/check-directory-existing.helper');

function downloadImage(link, imageName = 'noname', dowloadingPath) {
  checkDirectoryExisting(dowloadingPath);

  const file = fs.createWriteStream(`${dowloadingPath}/${imageName}`);
  return new Promise((resolve, reject) => {
    try {
      https.get(link, function (response) {
        response.pipe(file);
        resolve(true);
      });
    } catch (err) {
      reject(err);
    }
  });
}

module.exports.downloadImage = downloadImage;
