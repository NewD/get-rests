const needle = require('needle');

function httpsGet(link, successHandler) {
  console.log(
    'Request pizju link:',
    link,
    '\n___________________________________________________'
  );

  return new Promise((resolve, reject) => {
    try {
      needle.get(link, (error, response) => {
        if (error) throw error;

        if (!(successHandler instanceof Function)) {
          return resolve(response.body);
        }
        const result = successHandler(response.body);
        resolve(result);
      });
    } catch (err) {
      console.error(err);
      reject(err);
    }
  });
}

module.exports.httpsGet = httpsGet;
