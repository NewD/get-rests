class Resolver {
  _promise = null;
  _resolver = null;

  get value() {
    return this._promise;
  }
  get resolver() {
    return this._resolver;
  }

  constructor() {
    this._promise = new Promise((res) => (this._resolver = res));
  }
}

module.exports.Resolver = Resolver;
