const path = require('path');
const resultPath = path.resolve('', './src/slices/rests/_data');

module.exports.resultPath = resultPath;
module.exports.imagesPath = `${resultPath}/images`;
module.exports.jsonFileName = 'rests.json';
