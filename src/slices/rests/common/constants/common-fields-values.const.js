module.exports.commonFieldsValues = {
  type: null,
  description: null,
  address: null,
  city: null,
  phone: null,
  features: null,
  kitchen: null,
  photos: null,
  averageCheck: null,
};

module.exports.commonFieldNames = {
  name: 'name',
  type: 'type',
  description: 'description',
  address: 'address',
  city: 'city',
  phone: 'phone',
  features: 'features',
  kitchen: 'kitchen',
  photos: 'photos',
  averageCheck: 'averageCheck',
};
