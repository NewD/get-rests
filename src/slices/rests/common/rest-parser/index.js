const { Parser } = require('../../../../parser');
const { commonFieldsValues } = require('../constants/common-fields-values.const');

class RestParser extends Parser {
  rests = [];
  currentPage = 0;
  pagingHandler = null;
  linksParserParams = null;

  constructor(initSourceHTML = null, rootLink = '', additionalFields = {}) {
    super(
      initSourceHTML,
      rootLink,
      (additionalFields = { ...commonFieldsValues, ...additionalFields })
    );
  }

  parseLinks(cherioParams, options) {
    this.linksParserParams = { cherioParams, options };
    return super.parseLinks(cherioParams, options);
  }

  setupPagingHandler(handler) {
    this.pagingHandler = handler;
    return this;
  }

  parseRestsByLinks() {
    this._chainToAsyncResult(() => {
      this.links.forEach((link) => {
        this.updateHtmlByLink(this.rootLink + link).applyQueries();
      });
      return this;
    });

    if (!(this.pagingHandler instanceof Function)) return this;

    const { newPageLink, itemsLinksParserParams = this.linksParserParams } =
      this.pagingHandler(++this.currentPage);

    if (!newPageLink) return this;

    this._chainToAsyncResult(() => {
      this.updateHtmlByLink(newPageLink)
        .parseLinks(itemsLinksParserParams.cherioParams, itemsLinksParserParams.options)
        .parseRestsByLinks();

      return this;
    });

    return this;
  }
}

module.exports.RestParser = RestParser;
