const fs = require('fs');

const getRestsFromFile = (path) => JSON.parse(fs.readFileSync(path, 'utf-8'));

module.exports.getRestsFromFile = getRestsFromFile;
