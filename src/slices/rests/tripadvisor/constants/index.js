module.exports.pageSize = 30;
module.exports.rootLink = 'https://www.tripadvisor.ru';
module.exports.commonPageLink =
  'https://www.tripadvisor.ru/RestaurantSearch?Action=PAGE&ajax=1&availSearchEnabled=false&sortOrder=popularity&geo=298484&itags=10591&o=a';
