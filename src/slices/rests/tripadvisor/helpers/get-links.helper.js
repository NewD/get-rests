const { rootLink } = require('../constants');

function getLinks(rawLinks) {
  return Array.from(rawLinks, (link) => {
    const name = link.children.reduce((acc, child) => {
      return acc + child.data;
    }, '');
    const isNotAds = /^\d+\s+\.\s+/.test(name);
    const href = link.attribs.href;

    return { isNotAds, href };
  })
    .filter((item) => item.isNotAds)
    .map((item) => item.href);
}

module.exports.getLinks = getLinks;
