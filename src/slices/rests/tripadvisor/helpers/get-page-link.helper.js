const { commonPageLink } = require('../constants');

function getPageLink(page = 0, pageSize = 30) {
  return `${commonPageLink}${page * pageSize}`;
}

module.exports.getPageLink = getPageLink;
