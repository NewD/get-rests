const { commonFieldNames } = require('../../common/constants/common-fields-values.const');
const { RestParser } = require('../../common/rest-parser');
const { pageSize, rootLink } = require('../constants');
const { getLinks } = require('./get-links.helper');
const { getPageLink } = require('./get-page-link.helper');

const pageCount = 4;

async function getRests() {
  const parser = new RestParser(null, rootLink);

  const res = parser
    .updateHtmlByLink(getPageLink(0))
    .parseLinks('a.bHGqj.Cj.b', {
      handler: getLinks,
    })
    .addQuery(commonFieldNames.name, 'h1.fHibz')
    .setupPagingHandler((currentPage) => {
      return (
        currentPage < pageCount && { newPageLink: getPageLink(currentPage, pageSize) }
      );
    })
    .addResourceQuery(
      commonFieldNames.photos,
      'div.large_photo_wrapper img.basicImg, div.mini_mosaic_wrapper img.basicImg',
      {
        attr: 'data-lazyurl',
        withRootLink: false,
      }
    )
    .parseRestsByLinks()
    .asyncResult.then((e) => {
      console.log(e.resultArray);
    });
}

module.exports.getRests = getRests;
