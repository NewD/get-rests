const { getRests } = require('./helpers/get-rests.helper');
const { createJSONFile } = require('../../../helpers/create-json-file');
const { resultPath, jsonFileName } = require('../common/constants/result-path.const');

async function main() {
  const restsData = await getRests();

  // createJSONFile(restsData, resultPath, jsonFileName);
}

module.exports = main;
