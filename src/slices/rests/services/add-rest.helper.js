const axios = require('axios');

const FormData = require('form-data');
const fs = require('fs');
const { imagesPath } = require('../common/constants/result-path.const');

function addRest(restData) {
  restData.forEach((rest) => {
    const form = new FormData();

    Object.keys(rest).forEach((key) => {
      const item = rest[key];

      if (key === 'photos') {
        handlePhotos(item, form);
        return;
      }

      if (Array.isArray(item)) {
        handleArrayProp(item, key, form);
        return;
      }

      if (item && typeof item === 'object') {
        handleObjectProp(item, key, form);
        return;
      }

      form.append(key, item);
    });

    const formHeaders = form.getHeaders();

    axios({
      url: 'http://localhost:8080/businesses',
      method: 'post',
      data: form,
      headers: {
        ...formHeaders,
        Authorization:
          'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImFAbWFpbC5ydSIsImlkIjoiNjBjM2M4MjJhMGE4NDMyOWQyOGMxYzAxIiwiaWF0IjoxNjMyMTYwMDU3fQ.cxYEEX5MlaY6EXYKuMerwV5EOpO6zhPPQkQSQ5kS-bs',
      },
    }).catch((err) => console.log(err.message));
  });
}

function handleArrayProp(prop, propName, form) {
  prop.forEach((el) => {
    form.append(`${propName}[]`, el);
  });
}

function handleObjectProp(prop, propName, form) {
  for (let key in prop) {
    form.append(`${propName}[key]`, prop[key]);
  }
}

function handlePhotos(photos, form) {
  photos.forEach((photo) => {
    form.append('photos[]', fs.createReadStream(`${imagesPath}/${photo}`), {
      fileName: photo,
      contentType: 'image/jpeg',
    });
  });
}

module.exports.addRest = addRest;
