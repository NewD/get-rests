const axios = require('axios');

function deleteBusinesses() {
  axios.delete('http://localhost:8080/businesses/my', {
    headers: {
      Authorization:
        'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImFAbWFpbC5ydSIsImlkIjoiNjBjM2M4MjJhMGE4NDMyOWQyOGMxYzAxIiwiaWF0IjoxNjMyMTYwMDU3fQ.cxYEEX5MlaY6EXYKuMerwV5EOpO6zhPPQkQSQ5kS-bs',
    },
  });
}

module.exports.deleteBusinesses = deleteBusinesses;
