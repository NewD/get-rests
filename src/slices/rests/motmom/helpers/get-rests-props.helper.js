const { REST_OPTIONS_NAMES } = require('../constants/rest-options-names.enum');
const { removeBorderSpaces } = require('../../../../helpers/remove-border-spaces.helper');
globalThis.kitchen1 = [];

function getRestsProps(parsedRestData) {
  const delimeter = '|';
  const restInfo = parsedRestData.text().replace(/(\s\s)+/gm, delimeter);
  const optionsEnries = Object.entries(REST_OPTIONS_NAMES);
  const optionNames = Object.values(REST_OPTIONS_NAMES);

  return optionsEnries.reduce((acc, [optionName, optionTitle]) => {
    const optionValues = restInfo.match(
      new RegExp(`(?<=${optionTitle}:)[^]*?(?=${optionNames.join('|')}|$)`)
    );

    let rawResult = optionValues && optionValues[0];

    switch (optionTitle) {
      case REST_OPTIONS_NAMES.features: {
        const feturesEnum = {
          'Wi-Fi': 'WiFi',
          'Бизнес-ланч': 'Business_lunch',
          'Гостевая парковка': 'Parking',
          'Оплата картой': 'Card_pay',
          'Спортивные трансляции': 'Sport_streaming',
          'Летняя веранда': 'Terrace',
          'Доставка еды': 'Delivering',
          'Детская комната': 'Children_room',
        };

        const additionalFeatures = restInfo.match(
          new RegExp(`[^]*?(?=${optionNames.join('|')}|$})`)
        );

        const features = (additionalFeatures && additionalFeatures[0]) || '';
        rawResult = convertPropsKitToArray(
          rawResult ? rawResult + features : features,
          delimeter
        )
          .map((el) => feturesEnum[el])
          .filter((item) => !!item);

        break;
      }

      case REST_OPTIONS_NAMES.averageCheck: {
        if (!rawResult) return acc;
        const valueParts = rawResult.match(/\d+/g);

        rawResult = (Number(valueParts[0]) + Number(valueParts[1])) / 2;

        break;
      }

      case REST_OPTIONS_NAMES.kitchen: {
        const kitchenMap = {
          Европейская: 'European',
          Русская: 'Russian',
          Японская: 'Japanese',
          Итальянская: 'Italian',
          Мексиканская: 'Mexican',
          Фьюжн: 'Fusion',
          Грузинская: 'Georgian',
          Китайская: 'Chinese',
          Украинская: 'Ukrainian',
          Испанская: 'Spanish',
          Кавказская: 'Caucasian',
          Домашняя: 'Home_cooking',
          Французская: 'French',
          Узбекская: 'Uzbek',
        };

        rawResult = convertPropsKitToArray(rawResult, delimeter)
          .map((el) => kitchenMap[el])
          .filter((item) => !!item);

        break;
      }

      case REST_OPTIONS_NAMES.type: {
        rawResult = 'Restaurant';
        // rawResult?.match(
        //   new RegExp(`(?<=\\${delimeter}).+?(?=\\${delimeter})`, 'g')
        // )?.[0] ?? '';

        break;
      }
    }

    return { ...acc, [optionName]: rawResult ?? undefined };
  }, {});
}

function convertPropsKitToArray(str, delimetr) {
  if (!str) return [];
  return str
    .split(delimetr)
    .map(removeBorderSpaces)
    .filter((e) => !!e && e !== '...');
}

module.exports.getRestsProps = getRestsProps;
