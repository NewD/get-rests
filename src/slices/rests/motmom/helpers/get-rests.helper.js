const { commonFieldNames } = require('../../common/constants/common-fields-values.const');
const { RestParser } = require('../../common/rest-parser');
const { ROOT_HREF } = require('../constants/root-href.constant');
const { getRestsProps } = require('./get-rests-props.helper');

function getRests(rootLink) {
  const restParser = new RestParser(null, ROOT_HREF, { city: 'Пенза' });

  return restParser
    .updateHtmlByLink(rootLink)
    .parseLinks('div.info div.partner_name a', { linkOrder: 0 })
    .addQuery(commonFieldNames.name, 'div.partner_office_layout h1', {
      removeBorderSpaces: false,
    })
    .addQuery(commonFieldNames.address, 'div.block.address div.map_marker')
    .addQuery(commonFieldNames.phone, 'div.block.phones div.title', {
      handler: (rawResult) => ({
        [commonFieldNames.phone]: rawResult.text().replace(/\D/g, '').slice(0, 11),
      }),
    })
    .addQuery('', 'ul.filters_list div.filters_col', { handler: getRestsProps })
    .addQuery(commonFieldNames.description, 'div.office_description p')
    .addResourceQuery(commonFieldNames.photos, 'div.carousel_items img')
    .parseRestsByLinks().asyncResult;
}

module.exports.getRests = getRests;
