const { getRests } = require('./helpers/get-rests.helper');
const { createJSONFile } = require('../../../helpers/create-json-file');
const { resultPath, jsonFileName } = require('../common/constants/result-path.const');

const restsListLink = 'https://booking.motmom.com/penza/categories/restaurants';

async function main() {
  const { resultArray } = await getRests(restsListLink);

  console.log('globalThis.kitchen1', globalThis.kitchen1);

  createJSONFile(resultArray, resultPath, jsonFileName);
}

module.exports = main;
