const fs = require('fs');

function checkDirectoryExisting(dir) {
  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir, { recursive: true });
  }
}

module.exports.checkDirectoryExisting = checkDirectoryExisting;
