const fs = require('fs');
const { checkDirectoryExisting } = require('./check-directory-existing.helper');

function createJSONFile(data, path, fileName) {
  const jsonContent = JSON.stringify(data);

  const jsonFileName = fileName.endsWith('.json') ? fileName : fileName + '.json';
  checkDirectoryExisting(path);

  fs.writeFileSync(`${path}/${jsonFileName}`, jsonContent);
}

module.exports.createJSONFile = createJSONFile;
