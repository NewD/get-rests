function removeBorderSpaces(rawContent) {
  if (!rawContent) {
    return '';
  }

  if (typeof rawContent !== 'string') {
    return rawContent;
  }

  return rawContent.replace(/^\s*/gm, '').replace(/\s*$/gm, '');
}

module.exports.removeBorderSpaces = removeBorderSpaces;
