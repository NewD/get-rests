const {
  resultPath,
  jsonFileName,
} = require('./slices/rests/common/constants/result-path.const');
const { getRestsFromFile } = require('./slices/rests/helpers/get-rests-from-file.helper');
const motmom = require('./slices/rests/motmom');
const { addRest } = require('./slices/rests/services/add-rest.helper');
const { deleteBusinesses } = require('./slices/rests/services/delete-rest.helper');
const tripadvisor = require('./slices/rests/tripadvisor');

// motmom();

// tripadvisor();
addRest(getRestsFromFile(`${resultPath}/${jsonFileName}`));

// deleteBusinesses();
