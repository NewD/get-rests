const { Parser } = require('./index');

describe('Parser constructor', () => {
  let html;
  let parser;

  beforeEach(() => {
    html = '<div class="partner_office_layout">';
    parser = new Parser(html, '', { someField: null });
  });
  it('should setup additional fields', () => {
    expect(parser.parsedEntity['someField']).toBeDefined();
  });
});

describe('RestPareser.parseLinks', () => {
  let html;
  let parser;
  beforeEach(() => {
    html = `<div class="partners_offices_list">
    <div class="partner_offices_grid">
      <div class="partner_office_grid_item" item_id="17">
        <span class="cover_wrapper">
          <a href="/offices/17">
            <img
              src="/uploaded/offices/05/84/ab/a5/57/6c/42/de/0c/57/b0/42/47/76/a8/30/e6b2f7d3f53e02300e67b2fa61f8e808.jpeg"
              class="cover"
            />
            <div class="rating_wrapper">
              <div class="rating">
                <div class="default_stars">
                  <div class="active_stars" style="width: 96.8731563422%"></div>
                </div>
              </div>
            </div>
          </a>
          <a class="reservation-btn btn" href="/offices/17/reservation">Забронировать</a>
        </span>
      </div>
  
      <div class="partner_office_grid_item" item_id="13">
        <span class="cover_wrapper">
          <a href="/offices/13">
            <img
              src="/uploaded/offices/05/58/f5/4d/19/9b/a8/8f/76/a7/d3/52/6a/ac/33/f1/bf83eda05421bd398669ab3712e22ddb.jpeg"
              class="cover"
            />
            <div class="rating_wrapper">
              <div class="rating">
                <div class="default_stars">
                  <div class="active_stars" style="width: 95.0889679715%"></div>
                </div>
              </div>
            </div>
          </a>
          <a class="reservation-btn btn" href="/offices/13/reservation">Забронировать</a>
        </span>
      </div>
    </div>
  </div>
  `;
    parser = new Parser(html);
  });

  it('should be defined', () => {
    expect(parser.parseLinks).toBeInstanceOf(Function);
  });

  it('should return Parser', () => {
    expect(parser.parseLinks('div')).toBeInstanceOf(Parser);
  });

  it('should correctly parse links!', async () => {
    const result0 = ['/offices/17', '/offices/13'];
    const result1 = ['/offices/17/reservation', '/offices/13/reservation'];
    const result3 = [
      '/uploaded/offices/05/84/ab/a5/57/6c/42/de/0c/57/b0/42/47/76/a8/30/e6b2f7d3f53e02300e67b2fa61f8e808.jpeg',
      '/uploaded/offices/05/58/f5/4d/19/9b/a8/8f/76/a7/d3/52/6a/ac/33/f1/bf83eda05421bd398669ab3712e22ddb.jpeg',
    ];

    let asyncResult = await parser.parseLinks('span.cover_wrapper a', { linkOrder: 0 })
      .asyncResult;
    expect(asyncResult.links).toEqual(result0);

    asyncResult = await parser.parseLinks('span.cover_wrapper a', { linkOrder: 1 })
      .asyncResult;
    expect(asyncResult.links).toEqual(result1);

    asyncResult = await parser.parseLinks('img', { attr: 'src' }).asyncResult;
    expect(asyncResult.resourcesLinks).toEqual(result3);
  });
});

describe('Parser.parse', () => {
  let html;
  let parser;
  let handler;

  beforeEach(() => {
    html = '<div class="partner_office_layout"><span>Result</span></div>';
    parser = new Parser(html, '', { someField: null });
    handler = jest.fn(() => ({ someField: 'value' }));
    jest.spyOn(parser, '_applyParser');
    jest.spyOn(parser, '_applyResourceParser');
  });

  it('should be defined', () => {
    expect(parser.parse).toBeInstanceOf(Function);
  });

  it('should return Parser', () => {
    expect(parser.parse('someField', 'cherioParams')).toBeInstanceOf(Parser);
  });

  it('should correctly parse', () => {
    expect(parser.parse('someField', 'div span').parsedEntity.someField).toBe('Result');
  });

  it('should use handler if it has given', () => {
    expect(parser.parse('', 'div span', { handler }).parsedEntity.someField).toBe(
      'value'
    );
    expect(parser._applyParser).toBeCalledTimes(1);
    expect(handler).toBeCalledTimes(1);
  });
});

describe('Parser.updateHtmlSync', () => {
  let html;
  let parser;

  beforeEach(() => {
    html = '<div class="partner_office_layout">';
    parser = new Parser(html, ['someField']);
  });

  it('should throw error if source html is incorrect', () => {
    expect(() => parser.updateHtmlSync('<div')).toThrow();
  });

  it('should be defined', () => {
    expect(parser.updateHtmlSync).toBeInstanceOf(Function);
  });

  it('should return Parser', () => {
    expect(parser.updateHtmlSync('<div>text</div>')).toBeInstanceOf(Parser);
  });

  it("should update inner parser's dom", () => {
    expect(
      parser.updateHtmlSync('<div>some text</div>').parse('someField', 'div').parsedEntity
        .someField
    ).toBe('some text');
  });
});
