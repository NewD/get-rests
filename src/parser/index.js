const md5 = require('md5');
const htmlparser2 = require('htmlparser2');
const cheerio = require('cheerio');
const { downloadImage } = require('../services/download-image.service');
const { imagesPath } = require('../slices/rests/common/constants/result-path.const');
const { httpsGet } = require('../services/https-get.service');
const { Resolver } = require('../resolver');

class Parser {
  _queries = [];
  _resourcesQueries = [];

  links = [];
  resourcesLinks = [];

  parsedEntity = {
    name: null,
  };
  initParsedEntity = {};

  resultArray = [];
  _asyncResult = Promise.resolve(this);
  _result = new Resolver();
  _counter = 0;

  _resourcesEnum = {
    href: 'links',
    src: 'resourcesLinks',
    'data-lazyurl': 'resourcesLinks',
  };

  constructor(initSourceHTML, rootLink, additionalFields = {}) {
    if (initSourceHTML) {
      this.dom = htmlparser2.parseDocument(initSourceHTML);
      this.$ = cheerio.load(this.dom);
    }

    this.rootLink = rootLink;

    this.parsedEntity = { ...this.parsedEntity, ...additionalFields };
    this.initParsedEntity = { ...this.parsedEntity };
  }

  addQuery(fieldName, cherioParams, options = {}) {
    this._checkParserParams(fieldName, cherioParams, options);
    this._queries.push({ fieldName, cherioParams, options });

    return this;
  }

  addResourceQuery(fieldName, cherioParams, options = {}) {
    this._checkParserParams(fieldName, cherioParams, options);
    this._resourcesQueries.push({ fieldName, cherioParams, options });

    return this;
  }

  applyQueries() {
    const parsersSnapshot = [...this._queries];
    const resourcesParsersSnapshot = [...this._resourcesQueries];

    this._chainToAsyncResult(() => {
      this._applyParsersSync(parsersSnapshot, resourcesParsersSnapshot);
      this.resultArray.push({ ...this.parsedEntity });
      this.parsedEntity = { ...this.initParsedEntity };
      return this;
    });

    return this;
  }

  // TODO need to review!!!
  parseLinks(cherioParams, options = {}) {
    if (!cherioParams) {
      throw new Error('Sended empty cherioParams!');
    }

    this._chainToAsyncResult(() => {
      this._parseLinks(cherioParams, options);
      return this;
    });

    return this;
  }

  _parseLinks(cherioParams, options = {}) {
    const {
      // must return array of links
      handler = null,
      // linkOrder let u get only first, second or N' link in list
      // (if target block includes set of different links)
      linkOrder = -1,
      attr = 'href',
      overwrite = true,
    } = options;

    const safetyParams = typeof cherioParams === 'string' ? [cherioParams] : cherioParams;
    const rawResult = this.$(...safetyParams);
    const targetField = this._resourcesEnum[attr] ?? 'links';

    if (typeof handler === 'function') {
      this._handleOverwrite(handler(rawResult, this), targetField, overwrite);

      return;
    }

    this._handleOverwrite(
      this._getUniqueLinks(rawResult, attr, linkOrder),
      targetField,
      overwrite
    );

    return this;
  }

  get asyncResult() {
    return this._result.value;
  }

  updateHtmlSync(sourceHtml) {
    if (!sourceHtml) {
      throw new Error('Sended empty sourceHtml!');
    }

    this.sourceHtml = sourceHtml.slice(-2000).replace(/\n/gm, '');

    this.dom = htmlparser2.parseDocument(sourceHtml);
    if (
      (this.dom.type === 'root' && this.dom.prev === null,
      this.dom.next === null,
      this.dom.children.length === 0)
    ) {
      throw new Error('Sended incorrect HTML!');
    }

    this.$ = cheerio.load(this.dom);

    return this;
  }

  updateHtmlByLink(link) {
    this._chainToAsyncResult(() =>
      httpsGet(link, (content) => this.updateHtmlSync(content)).then(() => this)
    );

    return this;
  }

  removeParser(fieldName) {
    return this._remove(fieldName);
  }

  removeResourceParser(fieldName) {
    return this._remove(fieldName, 'resourcesParsers');
  }

  _remove(fieldName, parsersKitName = 'parsers') {
    if (!fieldName) {
      this[parsersKitName] = [];

      return this;
    }

    this[parsersKitName] = this[parsersKitName].filter(
      (parser) => parser.fieldName !== fieldName
    );

    return this;
  }

  _applyParsersSync(parsers = this._queries, resourcesParsers = this._resourcesQueries) {
    parsers.forEach((parser) => {
      this._applyParser(parser);
    });

    resourcesParsers.forEach((parser) => {
      this._applyResourceParser(parser);
    });
  }

  _applyParser = ({ fieldName, cherioParams, options }) => {
    const { handler = null, textOnly = true, removeBorderSpaces = true } = options;
    const safetyParams = typeof cherioParams === 'string' ? [cherioParams] : cherioParams;

    const rawResult =
      textOnly && !handler
        ? this._removeBorderSpaces(this.$(...safetyParams).text(), !removeBorderSpaces)
        : this.$(...safetyParams);

    if (typeof handler === 'function') {
      const newValues = handler(rawResult, this);

      Object.keys(newValues).forEach((key) => {
        if (!this._isFieldDefined(key)) {
          return;
        }

        this.parsedEntity[key] = newValues[key];
      });

      return;
    }

    this.parsedEntity[fieldName] = rawResult;
  };

  _applyResourceParser = ({ fieldName, cherioParams, options = {} }) => {
    const {
      extension = '.jpeg',
      maxCount = 3,
      handler = null,
      attr = 'src',
      withRootLink = true,
    } = options;
    if (!this.parsedEntity.name) {
      throw new Error("To parse resources u must setup entity's name!");
    }

    this._parseLinks(cherioParams, { handler, attr });
    this.parsedEntity[fieldName] = [];

    for (let index = 0; index < maxCount; index++) {
      const link = this.resourcesLinks[index];

      if (!link) {
        return;
      }

      const name = this._convertToResourceName(
        this.parsedEntity.name + md5(link),
        extension
      );
      this.parsedEntity[fieldName].push(name);

      // Todo rootLink logic need to be improved!!!
      const finalLink = withRootLink ? this.rootLink + link : link;
      downloadImage(finalLink, name, imagesPath);
    }
  };

  _removeBorderSpaces(rawContent, falseCall = false) {
    if (!rawContent) {
      return '';
    }

    if (falseCall || typeof rawContent !== 'string') {
      return rawContent;
    }

    return rawContent.replace(/^\s*/gm, '').replace(/\s*$/gm, '');
  }

  _checkParserParams(fieldName, cherioParams, options) {
    if (!options.handler && !this._isFieldDefined(fieldName)) {
      throw new Error(
        `Property '${fieldName}' is not defined in Parser! Please specify it when creating Parser instance!`
      );
    }

    if (!cherioParams) {
      throw new Error('Seems like u forgot to send cherioParams to parse method!');
    }
  }

  _isFieldDefined(fieldName) {
    return fieldName in this.parsedEntity;
  }

  _convertToResourceName(rawName, ext) {
    return rawName.replace(/\s*/g, '') + ext;
  }

  _getUniqueLinks(rawCherioItems, attr, linkOrder) {
    const rawLinks =
      linkOrder < 0
        ? Array.from(rawCherioItems, (el) => el.attribs[attr])
        : Array.from(rawCherioItems).reduce(
            (acc, el) => {
              let currentElNum;

              if (!acc.prevParent) {
                acc.prevParent = el.parent;
              }

              if (el.parent === acc.prevParent) {
                currentElNum = acc.itemNum++;
              } else {
                acc.prevParent = el.parent;
                acc.itemNum = 1;
                currentElNum = 0;
              }

              if (currentElNum !== linkOrder) {
                return acc;
              }
              acc.items.push(el.attribs[attr]);

              return acc;
            },
            { items: [], itemNum: 0, prevParent: null }
          ).items;

    return Array.from(new Set(rawLinks));
  }

  _handleOverwrite(dataArr, targetField, overwrite) {
    this[targetField] = overwrite ? dataArr : this[targetField].concat(dataArr);
  }

  _chainToAsyncResult(handler) {
    this._counter++;

    this._asyncResult = this._asyncResult.then(async () => {
      const a = await handler();
      this._counter--;
      if (this._counter === 0) {
        this._result.resolver(this);
      }

      return a;
    });
  }
}

module.exports.Parser = Parser;
